# **Banking Marketing Analysis**

This repository contains a dataset of banking marketing compaigns, along with Python scripts for data *cleaning*, *visualization*, *and analysis*.

## **What data set is this**

The dataset consists of over *45,000* records of customers who were contacted as part of a telemarketing campaign for a Prtuguese bank. Each record includes information such as the customer's age, job, marital status, education, and whether they subscribed to a term deposit or not.

## **My data cleaning process**

This is how I clean the data and stuff

1. Load data
2. stuff data
3. clean data

## **Goals of the Analysis**

The main goal of the analysis is to identify the factors that are most strongly associated with a customer subscribing to a term deposit. This information can be used to optimize future marketing compaigns and improve the bank's overall effectiveness.

# **Contents of the Repository**

* **bank.csv**: the original dataset in CSV format
* **data_cleaning.ipynb**: a Jupyter notebook containing Python code for visualizing the data
* **data_visualization.ipynb**: a Jupyter notebook containing Python code for visualizing the data
* **data_analysis.ipynb**: a Jupyter notebook containing Python code for analyzing the data
* **README.md**: this file 

## **How to use the repository**

To use this repository, simple clone it to your local machine and open the Jupyter notebooks in a Python environment such as Anaconda. 

* The codes used for pre-processing the data is available in project folder.

## **Quick Result**

### **Feature Distribution**

![Feature Distribution](feature_distribution.png)

##### **ROC curves of logistic regression model with features of degree 1, 2 and 3**

![ROC curves of logistic regression](ROC.png)

##### **Comparision of various classifiers**

![Comparison of classifiers](classifier.png)
 
# **Contact Information**

If you have any questions or feedback about this dataset or analysis, please feel free to contact me at < <aina.esenova88@gmail.com>

Thank you for considering my work!